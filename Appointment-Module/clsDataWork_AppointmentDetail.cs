﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Ontology_Module;
using Structure_Module;
using System.Threading;
using OntologyAppDBConnector;
using OntologyClasses.BaseClasses;

namespace Appointment_Module
{
    public class clsDataWork_AppointmentDetail
    {
        private clsLocalConfig objLocalConfig;
        private clsAppointment objAppointment;

        private OntologyModDBConnector objDBLevel_Contractees;
        private OntologyModDBConnector objDBLevel_Contractors;
        private OntologyModDBConnector objDBLevel_Watchers;
        private OntologyModDBConnector objDBLevel_Resources;

        private OntologyModDBConnector objDBLevel_Users;

        public clsOntologyItem OItem_Result_Contractees { get; set; }
        public clsOntologyItem OItem_Result_Contractors { get; set; }
        public clsOntologyItem OItem_Result_Watchers { get; set; }
        public clsOntologyItem OItem_Result_Resources { get; set; }

        private Thread objThread_Contacts;
        private Thread objThread_Resources;

        public SortableBindingList<clsContact> OList_Contacts { get; set; }
        public SortableBindingList<clsResource> OList_Resources { get; set; }
        

        


        public List<clsOntologyItem> OList_Users 
        { 
            get 
            {
                var OList_UserSearch = new List<clsOntologyItem>();

                OList_UserSearch.Add(new clsOntologyItem() { GUID_Parent = objLocalConfig.OItem_type_user.GUID });
                var OItem_Result = objDBLevel_Users.GetDataObjects(OList_UserSearch);

                return objDBLevel_Users.Objects1.OrderBy(obj => obj.Name).ToList();
            }
        }

        public clsDataWork_AppointmentDetail(clsLocalConfig LocalConfig)
        {
            objLocalConfig = LocalConfig;

            OList_Contacts = new SortableBindingList<clsContact>();

            OItem_Result_Contractees = objLocalConfig.Globals.LState_Nothing;
            OItem_Result_Contractors = objLocalConfig.Globals.LState_Nothing;
            OItem_Result_Watchers = objLocalConfig.Globals.LState_Nothing;

            initialize();
        }

        public void initialize_AppointmentDetail(clsAppointment Appointment)
        {
            OList_Contacts.Clear();
            try
            {
                objThread_Contacts.Abort();
                objThread_Resources.Abort();
            }
            catch (Exception ex)
            {
            }
            objAppointment = Appointment;
            if (objAppointment != null)
            {
                objThread_Contacts = new Thread(GetDate_Contacts);
                objThread_Contacts.Start();
                objThread_Resources = new Thread(GetData_Resources);
                objThread_Resources.Start();
            }

            
        }

        private void initialize()
        {

            objDBLevel_Contractees = new OntologyModDBConnector(objLocalConfig.Globals);
            objDBLevel_Contractors = new OntologyModDBConnector(objLocalConfig.Globals);
            objDBLevel_Watchers = new OntologyModDBConnector(objLocalConfig.Globals);

            objDBLevel_Users = new OntologyModDBConnector(objLocalConfig.Globals);
            objDBLevel_Resources = new OntologyModDBConnector(objLocalConfig.Globals);
        }

        private void GetDate_Contacts()
        {

            OItem_Result_Contractees = objLocalConfig.Globals.LState_Nothing;
            OItem_Result_Contractors = objLocalConfig.Globals.LState_Nothing;
            OItem_Result_Watchers = objLocalConfig.Globals.LState_Nothing;

            
            if (objAppointment != null)
            {
                GetData_Contractees();
                GetData_Contractors();
                GetData_Watchers();
            }
            else
            {
                OList_Contacts.Clear();
            }
            
        }

        public void GetData_Resources()
        {
            OItem_Result_Resources = objLocalConfig.Globals.LState_Nothing.Clone();
            OList_Resources = new SortableBindingList<clsResource>();

            var objOListS_Resources = new List<clsObjectRel> 
            { 
                new clsObjectRel 
                {
                    ID_Object = objAppointment.ID_Appointment, 
                    ID_RelationType = objLocalConfig.OItem_relationtype_located_at.GUID 
                },
                new clsObjectRel 
                {
                    ID_Object = objAppointment.ID_Appointment,
                    ID_RelationType = objLocalConfig.OItem_relationtype_belonging_resources.GUID
                } 
            };

            var objOItem_Result = objDBLevel_Resources.GetDataObjectRel(objOListS_Resources, doIds: false);

            if (objOItem_Result.GUID == objLocalConfig.Globals.LState_Success.GUID)
            {
                OList_Resources = new SortableBindingList<clsResource>(objDBLevel_Resources.ObjectRels.Select(r => new clsResource
                {
                    ID_Resource = r.ID_Other,
                    Name_Resource = r.Name_Other,
                    ID_Class_Resource = r.ID_Parent_Other,
                    Name_Class_Resource = r.Name_Parent_Other
                }).ToList());

                OItem_Result_Resources = objLocalConfig.Globals.LState_Success.Clone();
            }
            else
            {
                OItem_Result_Resources = objLocalConfig.Globals.LState_Error.Clone();
            }

        }

        

        private void GetData_Contractees()
        {
            var OList_Contractees = new List<clsObjectRel>();

            OList_Contractees.Add(new clsObjectRel()
            {
                ID_Object = objAppointment.ID_Appointment,
                ID_Parent_Other = objLocalConfig.OItem_type_partner.GUID,
                ID_RelationType = objLocalConfig.OItem_relationtype_belonging_contractee.GUID
            });

            var OItem_Result = objDBLevel_Contractees.GetDataObjectRel(OList_Contractees, 
                                                                                 doIds: false);

            if (OItem_Result.GUID == objLocalConfig.Globals.LState_Success.GUID)
            {
                if (objDBLevel_Contractees.ObjectRels.Any())
                {
                    OList_Contacts = new SortableBindingList<clsContact>(OList_Contacts.Concat((from obj in objDBLevel_Contractees.ObjectRels
                                                                                                select new clsContact()
                                                                                                {
                                                                                                    ID_Contact = obj.ID_Other,
                                                                                                    Name_Contact = obj.Name_Other,
                                                                                                    ID_Class_Contact = objLocalConfig.OItem_relationtype_belonging_contractee.GUID,
                                                                                                    Name_Class_Contact = objLocalConfig.OItem_relationtype_belonging_contractee.Name
                                                                                                }).ToList()));
                }
            }

            OItem_Result_Contractees = OItem_Result;
            
        }

        private void GetData_Contractors()
        {
            var OList_Contractors = new List<clsObjectRel>();

            OList_Contractors.Add(new clsObjectRel()
            {
                ID_Object = objAppointment.ID_Appointment,
                ID_Parent_Other = objLocalConfig.OItem_type_partner.GUID,
                ID_RelationType = objLocalConfig.OItem_relationtype_belonging_contractor.GUID
            });

            var OItem_Result = objDBLevel_Contractors.GetDataObjectRel(OList_Contractors,
                                                                                 doIds: false);

            if (OItem_Result.GUID == objLocalConfig.Globals.LState_Success.GUID)
            {
                if (objDBLevel_Contractors.ObjectRels.Any())
                {
                    OList_Contacts = new SortableBindingList<clsContact>(OList_Contacts.Concat((from obj in objDBLevel_Contractors.ObjectRels
                                                                                                select new clsContact()
                                                                                                {
                                                                                                    ID_Contact = obj.ID_Other,
                                                                                                    Name_Contact = obj.Name_Other,
                                                                                                    ID_Class_Contact = objLocalConfig.OItem_relationtype_belonging_contractor.GUID,
                                                                                                    Name_Class_Contact = objLocalConfig.OItem_relationtype_belonging_contractor.Name
                                                                                                }).ToList()));
                }
            }

            OItem_Result_Contractors = OItem_Result;
        }



        private void GetData_Watchers()
        {
            var OList_Watchers = new List<clsObjectRel>();

            OList_Watchers.Add(new clsObjectRel()
            {
                ID_Object = objAppointment.ID_Appointment,
                ID_Parent_Other = objLocalConfig.OItem_type_partner.GUID,
                ID_RelationType = objLocalConfig.OItem_relationtype_belonging_watchers.GUID
            });

            var OItem_Result = objDBLevel_Watchers.GetDataObjectRel(OList_Watchers,
                                                                           doIds: false);


            if (OItem_Result.GUID == objLocalConfig.Globals.LState_Success.GUID)
            {
                if (objDBLevel_Watchers.ObjectRels.Any())
                {
                    OList_Contacts = new SortableBindingList<clsContact>(OList_Contacts.Concat((from obj in objDBLevel_Watchers.ObjectRels
                                                                                                select new clsContact()
                                                                                                {
                                                                                                    ID_Contact = obj.ID_Other,
                                                                                                    Name_Contact = obj.Name_Other,
                                                                                                    ID_Class_Contact = objLocalConfig.OItem_relationtype_belonging_watchers.GUID,
                                                                                                    Name_Class_Contact = objLocalConfig.OItem_relationtype_belonging_watchers.Name
                                                                                                }).ToList()));
                }
            }

            OItem_Result_Watchers = OItem_Result;
        }
    }
}
